<?php
/*
  HISTORY:
    2018-03-21
      * minor updates for latest Ferreteria
      * now displays proper text on Special:SpecialPages
    2018-07-01 Changing $wgOptCP_Subst* globals to constants, because the globals weren't working
*/

class SpecialMakePage extends \SpecialPage {
    use \ferreteria\mw\tSpecialPage;

    // ++ SETUP ++ //

    // these should eventually be user-configurable
    const KS_OPT_SUBST_START = '[$';
    const KS_OPT_SUBST_FINISH = '$]';
    
    // OVERRIDE
    public function __construct() {
	parent::__construct('MakePage');
    }
    /*----
      OVERRIDE
      PURPOSE: Defines text to display on Special:SpecialPages
      NOTE: There's got to be a way to do this in extension.json, but I don't know what it is.
    */
    function getDescription() {
	return 'Make pages from form data';
    }
    
    // -- SETUP -- //
    // ++ EVENTS ++ //

    /*----
      NOTE: 2018-03-21 I'm not sure why the title isn't displaying in the proper place automatically.
	See '<h1 id="firstHeading"' in HTML source.
    */
    protected function Go() {
	global $wgRequest, $wgOut, $wgTitle;

	// collect required arguments:
	//$arArgs['!TITLETPLT']	= $wgRequest->getText('!TITLETPLT');	// template for new page's title
	//$arArgs['!TPLTPAGE']	= $wgRequest->getText('!TPLTPAGE');	// page to use as a template
	//$arArgs['!TPLTTEXT']	= $wgRequest->getText('!TPLTTEXT');	// text to use as a template
	
	$wgOut->AddHTML('<h1>Special:MakePage</h1>');		// KLUGE TODO: figure out proper way to display title
	$arArgs = $wgRequest->getValues();

	if (!$wgRequest->getBool('!TITLETPLT')) {
	    $urlDoc = 'http://htyp.org/MediaWiki/Special/MakePage';	// TODO: retrieve this from the configuration
	    $htArgs = \fcArray::Render($arArgs,1);
	    $out = <<<__END__
<p>This page requires specific arguments to be submitted from a form.
See <a href="$urlDoc">the documentation</a> for details.</p>
<p>Required arguments, as received:$htArgs</p>
<p>You must include !TITLETPLT and either !TPLTPAGE or !TPLTTEXT.
__END__;

	    $wgOut->AddHTML($out);	// display error diagnostic
	} else {
	    // collect optional arguments:
	    //$arArgs['!TPLTSTART']	= $wgRequest->getText('!TPLTSTART');	// optional starting marker
	    //$arArgs['!TPLTSTOP']	= $wgRequest->getText('!TPLTSTOP');	// optional stopping marker
	    //$arArgs['!IMMEDIATE']	= $wgRequest->getBool('!IMMEDIATE');	// optional: create page immediately
	    
	    $this->CreateWikiPage($arArgs);
	}
    }
    
    // -- EVENTS -- //
    // ++ OUTPUT ++ //

    protected function CreateWikiPage(array $arArgs) {
	global $wgOut, $wgTitle, $wgUser;
	global $wgErrorText;
	global $wxgDebug;

	$this->setHeaders();

	$sNewTitle	= $arArgs['!TITLETPLT'];		// template for new page's title
	$in_tpltpg	= fcArray::Nz($arArgs,'!TPLTPAGE');	// page to use as a template
	$in_tplttxt	= fcArray::Nz($arArgs,'!TPLTTEXT');	// text to use as a template
	$sDataStart	= fcArray::Nz($arArgs,'!TPLTSTART');		// optional template-contents starting marker
	$sDataStop	= fcArray::Nz($arArgs,'!TPLTSTOP');		// optional template-contents stopping marker
	$doImmediate	= fcArray::Nz($arArgs,'!IMMEDIATE',FALSE);	// optional: create page immediately

	$referer	= $_SERVER['HTTP_REFERER'];		// name of page which sent the form data
// get the text to be used as a template:
	if ($in_tpltpg) {
	    // if it's stored in a page, load that (overrules direct specification):
	    $mwoTplt = Title::newFromText($in_tpltpg);
	    $wgOut->AddWikiText("'''Template page''': [[$in_tpltpg]]");
	    //$mwoArtcl = new Article($mwoTplt);
	    $mwoPage = new WikiPage($mwoTplt);
	    $mwoContent = $mwoPage->getContent();
	    
	    //$sNewText = $mwoArtcl->getContent();
	    //$mwoContent = $mwoArtcl->getContentObject();
	    $sNewText = $mwoContent->getWikitextForTransclusion();	// I hope this works...
	} else {
	    $sNewText = $in_tplttxt;
	}

// truncate before/after markers, if any:
	if (!is_null($sDataStop)) {
	    $posSt = strpos ( $sNewText, $sDataStop );
	    if ($posSt !== FALSE) {
		$sNewText = substr($sNewText,0,$posSt);
	    }
	}
	if (!is_null($sDataStart)) {
	    $posSt = strpos ( $sNewText, $sDataStart );
	    if ($posSt !== FALSE) {
		$sNewText = substr($sNewText,$posSt+strlen($sDataStart));
	    }
	}

// do variable swapout:
	//$objTplt = new clsStringTemplate_MWRequest($wgOptCP_SubstStart,$wgOptCP_SubstFinish,$wgOptCP_SubstSetVal);
		
	$oTplt = new fcTemplate_array(self::KS_OPT_SUBST_START,self::KS_OPT_SUBST_FINISH,$sNewText);
	$oTplt->SetIgnoreCase(TRUE);
	$oTplt->SetVariableValues($arArgs);
	// calculate contents for new page
	//$oTplt->Value = $sNewText;
	//$sNewText = $oTplt->Replace();
	try {
	    $sNewText = $oTplt->RenderRecursive();
	} catch (Exception $e) {
	    $oTrace = new fcStackTrace($e);
	    $out = 
	      fcApp::Me()->MessagesString()
	      .$e->getMessage()
	      .'<br><b>Stack trace</b>:'
	      //.fcArray::Render($e->getTrace())
	      .$oTrace->RenderAllRows()
	      ;
	    $wgOut->addHTML($out);
	}

	// calculate title for new page
	$sOldTitle = $sNewTitle;
	$oTplt->Template($sOldTitle);
	$sNewTitle = $oTplt->Render();
	
	/* 2018-07-01 DEBUGGING
	$wgOut->addHTML('ARGS:'.fcArray::Render($oTplt->GetVariableValues()));
	$wgOut->addHTML("TITLE TEMPLATE: [$sOldTitle] RESULT: [$sNewTitle] START=&lt;"
	  .self::KS_OPT_SUBST_START."&gt; FINISH=&lt;"
	  .self::KS_OPT_SUBST_FINISH."&gt;");
	*/

	$wgOut->setPageTitle( $sNewTitle );
/*
2008-01-22 For future reference: we might want to just remove any characters not found in $wgLegalTitleChars (=Title::legalChars())
	Sometimes CRs or TABs get pasted invisibly into the title, causing mysterious inability to create the page.
*/
	$mwoNewTitle = Title::newFromText( $sNewTitle );
	if (is_object($mwoNewTitle)) {
	    $sEditSumm = "Created by MakePage, submitted from [$referer].";

	    $doPreview = !$doImmediate;	// if not creating page immediately, preview it first
	    if ($doPreview) {
		$mwoNewArticle = new Article($mwoNewTitle);
		$mwoNewArticle->mContent = $sNewText;
		//$mwoxNewEdit = new EditOtherPage($mwoNewArticle);
		$mwoNewEdit = new EditPage($mwoNewArticle);
		$strNewTitle_check = $mwoNewEdit->mTitle->getPrefixedText();
		$wgOut->AddWikiText("'''New Page Title''': [[$strNewTitle_check]]");
		$wgOut->AddWikiText("'''Preview''': <hr>\n$sNewText<hr>");

		$mwoNewEdit->textbox1 = $sNewText;
		$mwoNewEdit->preview = $doPreview;
		$wgOut->AddWikiText("'''Make final changes to the text here, and save to create the page:'''");
		$wgTitle = $mwoNewTitle;	// make the form post to the page we want to create
		$mwoNewEdit->action = 'submit';
		$mwoNewEdit->contentModel = CONTENT_MODEL_WIKITEXT;
		$mwoNewEdit->summary = $sEditSumm;
		$mwoNewEdit->showEditForm();
	    } else {
		// IMMEDIATE mode (no preview, just create the page):

		//* 2015-09-15 This code does create a new page with the correct contents,
		  // but it has to be manually purged before tags are parsed.

		$mwoPage = new WikiPage($mwoNewTitle);
		$mwoPage->doEdit($sNewText,$sEditSumm,EDIT_NEW);

	    }
	} else {
	    $txtOut = 'There seems to be a problem with the title: [['.$sNewTitle.']]';
	    if ($wgErrorText) {
		    $txtOut .= ': ' . $wgErrorText;
	    }
	    $txtOut .= "\n\n".$wxgDebug;
	    $wgOut->AddWikiText($txtOut);
	}
	$wgOut->returnToMain( false );
    }

    // -- OUTPUT -- //

}
